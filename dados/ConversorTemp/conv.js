
var btnConverter = document.querySelector("#btnConverter")


btnConverter.onclick = function (e) {
    e.preventDefault();
    let tempEntrada = Number(document.getElementById("idTemp").value)
    let unidadeEntrada = document.querySelector("#idUnidadeOrigem").value
    let unidadeConvesao = document.querySelector("#idUnidadeConvertido").value
    //Conversao da unidade de entrada para Celsius 
    let tempCesius
    var temporaria
    switch (unidadeEntrada) {
        case "C":
            tempCesius = tempEntrada
            break;

        case "F":
            tempCesius = (tempEntrada*1,8) + 32
            temporaria = tempCesius;

        case "K":
            tempCesius = tempEntrada - 273
            break;

        default:
            break;
    }

    
    //Conversao de celsius para a unidade de saida 
    let tempConvertido
    switch (unidadeConvesao) {
        case "C":
            tempConvertido = tempCesius
            break;
        case "F":
            tempConvertido = (9*tempCesius/5) + 32
            break;
        case "K":
            tempConvertido = tempCesius + 273
            break;
        default:
            break;
    }

    if(unidadeEntrada==unidadeConvesao){
        tempConvertido = tempEntrada
    }
    if(unidadeConvesao=="C"){
        console.log("entrei");
        tempConvertido = tempCesius
        if(unidadeEntrada=="F"){
            tempConvertido = temporaria
        }
    }

    document.getElementById("idTempConvertido").value = tempConvertido


}
